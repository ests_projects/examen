<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Web2market\Surepost\Model\Config\Source;

/**
 * Class Method
 */
class Method extends \Web2market\Surepost\Model\Config\Source\Generic
{
    /**
     * Carrier code
     *
     * @var string
     */
    protected $_code = 'method';
}
